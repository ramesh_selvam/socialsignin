import AuthenticationServices
import FacebookLogin
import TwitterKit
@available(iOS 13.0, *)
public class SocialSignin: NSObject  {
    
    public static let shared = SocialSignin()
    
    
    //MARK:- AppleID
    
    var apple: SigninAppleId?
    
    public func getSigninWithAppleIdButton(delegate: SigninAppleIdDelegate ) -> ASAuthorizationAppleIDButton
    {
        apple = SigninAppleId()
        return apple!.getSigninAppleIdButton(delegate: delegate)
    }
    
    func handleAuthorizationAppleIDButtonPress(delegate: SigninAppleIdDelegate) {
        apple = SigninAppleId()
        return apple!.handleAuthorizationAppleIDButtonPress(delegate: delegate)
        
    }
    

    //MARK:- FaceBook
    
    var faceBook: SigninFaceBook?
    var signinWithFaceBookDelegate: SigninFaceBookDelegate?
    
    public func getSigninWithFaceBookdButton(delegate: SigninFaceBookDelegate ) -> FBLoginButton
    {
        faceBook = SigninFaceBook()
        signinWithFaceBookDelegate = delegate
        self.faceBook?.delegate = delegate
        return faceBook!.getSigninWithFaceBookButton(delegate: delegate)
    }
    
    
    
    //MARK:- Twitter
    
    var twitter: SigninTwitter?
    private var twitterIsStarted = false
    
    public  func registerWithTwitter() {
        twitter = SigninTwitter()
        twitter?.start()
        twitterIsStarted = true
    }
    
    func signinWithTwitterButtonAction(completion: @escaping TWTRLogInCompletion) {
        
        if !self.twitterIsStarted{
            self.registerWithTwitter()
        }
        
        self.twitter?.siginButtonAction(completion: completion)
    }
    
    
    
    //MARK:- Google
    
    var google = SigninGoogle()
    
    func registerGoogle(delegate: SigninGoogleDelegate){
        self.google.register(delegate: delegate);
    }
    
    func setGooglepresentingViewController(viewController: UIViewController){
        self.google.setGooglepresentingViewController(viewController: viewController)
    }
    
    func restorePreviousSignIn(){
        self.google.restorePreviousSignIn()
    }
    
}




 
