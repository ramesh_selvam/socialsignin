//
//  File.swift
//  
//
//  Created by RK on 24/07/20.
//

import AuthenticationServices


public protocol SigninAppleIdDelegate {
    
     func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization)

     func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error)

}

@available(iOS 13.0, *)

class SigninAppleId: NSObject {

    var delegate: SigninAppleIdDelegate?
    
    func getSigninAppleIdButton(delegate: SigninAppleIdDelegate) -> ASAuthorizationAppleIDButton  {
        self.delegate = delegate
        keyChainAccess()
        let authorizationButton = ASAuthorizationAppleIDButton()
        authorizationButton.addTarget(self, action: #selector(appleIDButtonPress), for: .touchUpInside)
        return authorizationButton
    }
    
    
    func performExistingAccountSetupFlows() {
        let requests = [ASAuthorizationAppleIDProvider().createRequest(),
                        ASAuthorizationPasswordProvider().createRequest()]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    /// - Tag: perform_appleid_request
    @objc
   private func appleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    
    
    func handleAuthorizationAppleIDButtonPress(delegate: SigninAppleIdDelegate) {
        self.delegate = delegate
        appleIDButtonPress()
    }

    
    func keyChainAccess(){
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: KeychainItem.currentUserIdentifier) { (credentialState, error) in
            switch credentialState {
            case .authorized:
                break // The Apple ID credential is valid.
            case .revoked, .notFound: break
                
                // The Apple ID credential is either revoked or was not found, so show the sign-in UI.
//                DispatchQueue.main.async {
//                    self.window?.rootViewController?.showLoginViewController()
//                }
            default:
                break
            }

    }
}
}



extension SigninAppleId: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        self.delegate?.authorizationController(controller: controller, didCompleteWithAuthorization: authorization)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        self.delegate?.authorizationController(controller: controller, didCompleteWithError: error)
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        let vc = delegate as! UIViewController
        return vc.view.window!
    }
    
    
}
