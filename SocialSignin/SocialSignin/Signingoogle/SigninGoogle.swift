//
//  SigninGoogle.swift
//  SocialSignin
//
//  Created by RK on 05/08/20.
//  Copyright © 2020 Rameshkumar Selvam. All rights reserved.
//

import UIKit
import GoogleSignIn

public protocol SigninGoogleDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!)
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!)
    
}

class SigninGoogle: NSObject {
    
    // please update valid googleClientId 
    
    let googleClientId = "215883508682-5ak4c1o2ti5lbq00s6jagqk39a0gkcnp.apps.googleusercontent.com"
    
    var delegate: SigninGoogleDelegate?
    
    func register(delegate: SigninGoogleDelegate){
        
        GIDSignIn.sharedInstance().clientID = googleClientId
        self.delegate = delegate
        GIDSignIn.sharedInstance().delegate = self
        
        
    }
    
    func restorePreviousSignIn() {
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
    
    func setGooglepresentingViewController(viewController: UIViewController){
        GIDSignIn.sharedInstance()?.presentingViewController = viewController
        
    }
}


extension SigninGoogle: GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        self.delegate?.sign(signIn, didSignInFor:user, withError:error)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
        self.delegate?.sign(signIn, didDisconnectWith:user, withError:error)
    }
    
}
