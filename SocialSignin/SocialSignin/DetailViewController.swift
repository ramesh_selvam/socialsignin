//
//  ViewController.swift
//  SiginApp
//
//  Created by RK on 23/07/20.
//  Copyright © 2020 RK. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var userInfo: Dictionary<String, Any>?
    static func instantiateViewController(userInfo: Dictionary<String, Any>) -> DetailViewController
    {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc: DetailViewController = sb.instantiateViewController(identifier: "DetailViewController")
        vc.userInfo = userInfo
        return vc;
        
    }
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var detaiTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let dictionary = self.userInfo
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dictionary as Any,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                       encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            detaiTextView.text = theJSONText
        }

        logoImageView.image = UIImage.init(named: userInfo!["signinType"] as! String)

    }


}

