//
//  SigninViewController.swift
//  SiginApp
//
//  Created by RK on 23/07/20.
//  Copyright © 2020 RK. All rights reserved.
//

import UIKit

import AuthenticationServices
import FacebookCore
import FacebookLogin
import GoogleSignIn
import TwitterKit
class SigninViewController: UIViewController {
    
    @IBOutlet weak var loginProviderStackView: UIStackView!
    @IBOutlet weak var signInButton: GIDSignInButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProviderLoginView()

        SocialSignin.shared.registerGoogle(delegate: self);
        SocialSignin.shared.setGooglepresentingViewController(viewController: self)
        SocialSignin.shared.registerWithTwitter()
        
    }
    
    
    func setupProviderLoginView() {
        
        //Apple id
        let appleIDLoginButton = SocialSignin.shared.getSigninWithAppleIdButton(delegate: self)
        
        //face book
        let fbLogInButton = SocialSignin.shared.getSigninWithFaceBookdButton(delegate: self)
        
        // Twitter
        let twitterLogInButton = TWTRLogInButton(logInCompletion: { session, error in
            if (session != nil) {
                print("signed in as \(session!.userName)");
            } else {
                print("error: \(error!.localizedDescription)");
            }
        })
        
        self.loginProviderStackView.addArrangedSubview(appleIDLoginButton)
        self.loginProviderStackView.addArrangedSubview(fbLogInButton)
        self.loginProviderStackView.addArrangedSubview(twitterLogInButton)
        
    }

    
    @IBAction func didTapSignOut(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    
    
    @IBAction func didTapfacebookButton(_ sender: AnyObject) {
        getFacebookUserInfo()
        
    }

    
    func getFacebookUserInfo(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email ], viewController: self) { (result) in
            switch result{
            case .cancelled:
                print("Cancel button click")
            case .success:
                let params = ["fields" : "id, name, first_name, last_name, email "]
                GraphRequest(graphPath: "me", parameters: params ).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        print(result as! Dictionary <String, Any>)
                        var dic = result as! Dictionary <String, Any>
                        dic["signinType"] = "fb"
                        self.loadDetailVC(userInfo: dic)
                    }
                })
            default:
                print("??")
            }
        }
    }

    
    var firstName = ""
    var lastName = ""
    
    
    @IBAction func tapApple_btn(_ sender: Any) {
        
        SocialSignin.shared.handleAuthorizationAppleIDButtonPress(delegate: self)
    }

    @IBAction func tapTwitter_btn(_ sender: Any) {
        
        SocialSignin.shared.signinWithTwitterButtonAction( completion: { (session, error) in
            if (session != nil) {
                self.firstName = session?.userName ?? ""
                self.lastName = session?.userName ?? ""
                let client = TWTRAPIClient.withCurrentUser()
                client.requestEmail { email, error in
                    if (email != nil) {
                        print("signed in as \(String(describing: session?.userName))");
                        let firstName = session?.userName ?? ""   // received first name
                        let lastName = session?.userName ?? ""  // received last name
                        let recivedEmailID = email ?? ""   // received email
                        
                        
                    }else {
                        print("error: \(String(describing: error?.localizedDescription))");
                    }
                }
            }else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        })
    }
    
    
    func loadDetailVC(userInfo: Dictionary<String, Any>)
    {
        DispatchQueue.main.async {
    let vc =  DetailViewController.instantiateViewController(userInfo: userInfo)
    self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    
}

extension SigninViewController: SigninFaceBookDelegate
{
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        if((AccessToken.current) != nil){
            
            // you can customise params dict 
            // for more info https://developers.facebook.com/docs/graph-api/reference/user

            let params = ["fields": "id, name, first_name, last_name, email"]

            GraphRequest(graphPath: "me", parameters: params ).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! Dictionary <String, Any>)
                    var dic = result as! Dictionary <String, Any>
                    dic["signinType"] = "fb"
                    self.loadDetailVC(userInfo: dic)
                }
            })
        }
        print(result?.description as Any)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
    
    
}
extension SigninViewController: SigninAppleIdDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            var dic:  Dictionary <String, Any> = [:]
            dic["signinType"] = "apple"
            dic["userIdentifier"] = userIdentifier
            dic["familyName"] = fullName?.familyName
            dic["email"] = email ?? ""
            self.loadDetailVC(userInfo: dic)
            
        case let passwordCredential as ASPasswordCredential:
            
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password

            
        default:
            break
        }
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {

        print(error.localizedDescription)
    }

}

extension SigninViewController: SigninGoogleDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        var dic:  Dictionary <String, Any> = [:]
        dic["signinType"] = "google"
        dic["userId"] = userId
        dic["idToken"] = idToken
        dic["fullName"] = fullName
        dic["givenName"] = givenName
        dic["familyName"] = familyName
        dic["email"] = email
        self.loadDetailVC(userInfo: dic)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
}
