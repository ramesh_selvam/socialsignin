//
//  SigninFaceBook.swift
//  Sample
//
//  Created by RK on 27/07/20.
//  Copyright © 2020 impiger. All rights reserved.
//

import UIKit
import FBSDKLoginKit

public protocol SigninFaceBookDelegate {
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?)
    func loginButtonDidLogOut(_ loginButton: FBLoginButton)

}

class SigninFaceBook: NSObject {
    
    let loginButtonPermissions = ["public_profile", "email"]

    var delegate: SigninFaceBookDelegate?
    public func getSigninWithFaceBookButton(delegate: SigninFaceBookDelegate ) -> FBLoginButton
    {
        self.delegate = delegate
        let loginButton = FBLoginButton()
        loginButton.delegate = self
        loginButton.permissions = loginButtonPermissions
        return loginButton
    }
    
    public func userAlreadySigninWithFaceBook() -> Bool{
        
        if let token = AccessToken.current{
            
            return (!token.isExpired)
        }
        else
        {
            return false
        }
    }
    
}

extension SigninFaceBook: LoginButtonDelegate
{
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        self.delegate?.loginButton(loginButton, didCompleteWith:result, error:error)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
        self.delegate?.loginButtonDidLogOut(loginButton)
    }
    
    
    func loginButtonWillLogin(_ loginButton: FBLoginButton) -> Bool {
        
        return true
        }

}
