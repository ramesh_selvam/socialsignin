# Depictron

_Form framework XML schema definitions_



# Form layout structure

A form layout should contains following tags

1. [Form Layout <formLayout>](#Label)
2. [Page <page>](#_xkgn70gkc1tv)
3. [Section <section>](#_tyain1cf9qmc)
4. [Widget <widget>](#_q09dy3r4ynh7)

## Form Layout <formLayout>

This is the root tag of form

### Form layout attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| formId (mandatory) | String | None | The unique identifier of the form.ex: <form id="form003".. |
| formName(optional) | String | empty | The name of the formex: <form id ="form02" formName="Customer Registration" |

A form consists of one or more pages.

**Example:**

	'***' <formLayout id ="31200" formName="Customer Registration"> ***
	
			<page id ="page0045" pageName="Personal information">
			
		 		 …….
				 
		 		 …….
				 
		  	</page>
			
		</formLayout>
	

## Page <page> 

Page represents a single screen of device. It holds the collection widgets and sections. A page has no idea of its orientation (only its internal sections define the orientation)

### Page attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| pageId(mandatory) | String | None | The unique identifier of the page.ex: <page id="3100".. |
| pageName (optional) | String | empty string | The name of the formex: <page id ="3100" pageName="Contact Information" |
| pageNum(optional) | Numeric | empty string | The page number valueex<page id ="3100" pageNo="8" |

**Example:**

			<formLayout id ="31200" formName="Customer Registration">

				**<page id ="0045" pageName="Personal information">**

					<section orientation="horizontal">

						<widget/>

						<widget/>

					</section>

					<widget/>

				</page>

			</formLayout>

## Section<section>

Section is basic structure layout of the Form or Page. A section contains widgets or sections. It arranges its children in a single column or a single row

### Section attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| id (mandatory) | String | None | The unique identifier of the section. |
| orientation(optional) | [OrientationType](#_ce41mu8pxdv6) | horizontal | This section should be a column or a row. Possible values " **horizontal**" or " **vertical**"horizontal- Arranges its children horizontally on the pagevertical- Arranges its children vertically on the page ex: <section orientation="horizontal" |
| dividePercentage(optional) | Numeric | 100 | This value specifies how much space this section can occupy if it participates inside another section with siblings (sections or widgets). value range should be in 1 to 100default value is 100 ex: <section orientation="horizontal" " dividePercentage ="30"... This value is ignorable if it is not present in a section or its siblings are invisible (visible=hide) or no siblings |
| visible(optional) | [VisibleOptionType](#_6tt791ozvpw8) | show | Controls visibility of widget.String - either value of " **show**" or " **hide**". Default value is "show"ex: <widget id ="2345" visible="hide" |
| expandable | [YesOrNoOptionsType](#_22794bqmt7qc) | no | It is a special type of[Section](https://docs.google.com/document/d/1TG2DBQn2kJoNJhj5pDYtQVGvz4pPYQuYG __zfE__ FuU/edit#heading=h.qswcw7bo9wwp). It has a section header title with additional functionality that we can expand and collapse the section view through the button at right side of the section header or "onClick" on the section header. |
| sectionTitle | String | empty | The section hear title text. |

**Example:** 

			<formLayout id ="31200" formName="Customer Registration">

			<page id ="0045" pageName="Personal information">

				**<section id="address\_section01"orientation= "horizontal">**

					<widget/>

					<widget/>

				</section>

				<widget/>

			</page>

			</formLayout>

## Widget: <widget>

Widget is an informative element of a form. User interactive part of the form.

### Widget attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| id(mandatory) | String | None | The unique identifier of the widget.ex: <widget id ="2345"....> |
| type(mandatory) | [WidgetType](#_f4f4api53swj) | None | View mode or widget type. This should be any one of the view models that already described. |
| visible(optional) | [VisibleOptionType](#_6tt791ozvpw8)| show | Controls visibility of widget.String - either value of " **show**" or " **hide**". Default value is "show"ex: <widget id ="2345" visible="hide" |
| dividePercentage(optional) | Numeric | 100 | This value specify how much space is should occupy in section if two more widgets exists in same section. Value should be in 1 to 100 ex: <widget id ="2345" dividePercentage ="30" This value is ignorable if it is not present in a section or it's siblings are invisible (visible=hide)> |
| contentDescription(optional) | String | Empty |
 

**Example:**

			<formLayout formId="form001" formName="Registration">

				<page pageId="page001" pageName="Personal Information" pageNum="1">

					<section orientation="horizontal">
					
						<widget id="001" dividePercentage="50" type="label" displayText="First Name" visible="hide"/>
						
					</section>
					
				</page>
				
			<formLayout>

## Widget Type specific attributes

### Label

 It's a non editable field used to display text or message.

##### Label attributes

| **Attributes** | **Data type** | **Default** | **Description** |
| --- | --- | --- | --- |
| displayText(mandatory) | String | None | The text to display |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |

### EditText

Its an editable text field.

##### EditText attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| hintText(optional) | String | empty string | The hint text to display in edit field until user starts editing. |
| inputType(optional) | [InputTextStyle](#_lyb2k54ukc3h) | text | The input type of text |
| defaultText(optional) | String | empty string | The default value of the edit if user is not editing This default value should match the input type |
| mandatory(optional) | [YesOrNoOptionsType](#_22794bqmt7qc) | no | Consider the field as mandatory.Value should be **"yes" or "no"** Default value is "no" |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |
| multiLine | [YesOrNoOptionsType](#_22794bqmt7qc) | no | Allows multiple lines of text in EditText |
| lines | Numeric | 0 | If multiLine is "yes", this value Makes the TextView be exactly this many lines tall. |

### CheckBox

A checkbox is a specific type of two-states widget that can be either checked or unchecked.

##### CheckBox attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| displayText(mandatory) | String | None | Check box title text |
| state(optional) | String | unchecked | The default state of the checkboxValue should be **"checked" or "unchecked"** |
| checkboxPosition(optional) | String | string | Show checkbox before message text or after text " **right" or "left"** |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |

### ChoiceBox

This shows collection items or options on form. User can pick one or multiple items based on type..

The list of options specified in <itemSequence>

##### ChoiceBox Attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| displayText(mandatory) | String | None | The text to displayex <checkbox displayText ="Do you want to service sms"... |
| choiceBoxType(optional) | [ChoiceBoxStylesType](#_fqba71rjlsl) | list | The type of choiceboxSpinner listCheckbox list |
| defaultChoice(optional) | Integer | None | The index of array of items |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |

## ChoiceBox options Items

The list options data used in choiceBox are specified by following formats

- Options list <itemSequence>

**Options list <itemSequence>**

List options specified as sequence in <item> tag

| **Attributes** | **Data type** | **Default** | **Description** |
| --- | --- | --- | --- |
| modelBindId(optional) | String | None | The unique id to represent a model object Data of specific widget can be get from given model or extracted data from the widget to be apply the given model |
| arrangeOrder | [arrangeOrderOptionsType](#_wjny23z48g6r) | sameOrder | Defines the display style or order. The way of arrange the options in the choice Box |

**Item**

The individual value is specified in <item>

| **Attributes** | **Data type** | **Default** | **Description** |
| --- | --- | --- | --- |
| displayName(mandatory) | String | None | The string value for the item |
| internalValue | String | None | The internal representation of the displayData . Its mab be the model object id or index value of item |

**Example:**

			<itemSequence id="" arrangeOrder="alphabetic">

				<item displayName="debit card" internalValue="1" />

				<item displayName="credit card" internalValue="2" />

				<item displayName="net banking" internalValue="3" />

			</itemSequence>

### Date

It is an editText with a clickable date picker icon at right end of the editText. This picker icon "onClick" invokes[DatePickerDialog](http://developer.android.com/reference/android/app/DatePickerDialog.html). We can insert Date to editText field through this dialog.

##### Date attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| hintText(optional) | String | empty string | The hint text to display in edit field until user starts editing. |
| defaultText(optional) | String | empty string | The default value of the edit if user is not editing This default value should match the input type |
| mandatory(optional) | [YesOrNoOptionsType](#_22794bqmt7qc) | no | Consider the field as mandatory.Value should be **"yes" or "no"** Default value is "no" |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |

### Time

It is an editText with a clickable date picker icon at right end of the editText. This picker icon "onClick" invokes[DatePickerDialog](http://developer.android.com/reference/android/app/DatePickerDialog.html). We can insert Date to editText field through this dialog.

##### Time attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| hintText(optional) | String | empty string | The hint text to display in edit field until user starts editing. |
| defaultText(optional) | String | empty string | The default value of the edit if user is not editing This default value should match the input type |
| mandatory(optional) | [YesOrNoOptionsType](#_22794bqmt7qc) | no | Consider the field as mandatory.Value should be **"yes" or "no"** Default value is "no" |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |

### DateTime

It is an editText with a clickable date picker icon at right end of the editText. This picker icon "onClick" invokes[DatePickerDialog](http://developer.android.com/reference/android/app/DatePickerDialog.html). We can insert Date to editText field through this dialog.

##### Date attributes

| **Attributes** | **Data Type** | **Default value** | **Description** |
| --- | --- | --- | --- |
| hintText(optional) | String | empty string | The hint text to display in edit field until user starts editing.The hint text is single string with "&amp;" separator. Ex "Starting date" &amp; "Starting time" |
| defaultText(optional) | String | empty string | The default value of the edit if user is not editing This default value should match the input typeThe default value is string with "&amp;" separator. Ex "01-Aug-2011" &amp; "10.30 am" |
| mandatory(optional) | [YesOrNoOptionsType](#_22794bqmt7qc) | no | Consider the field as mandatory.Value should be **"yes" or "no"** Default value is "no" |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |

### Button

It's a non editable field used to display text or message.

##### Button attributes

| **Attributes** | **Data type** | **Default** | **Description** |
| --- | --- | --- | --- |
| displayText(mandatory) | String | None | The text to display |
| displayTextStyle(optional) | [TextStylesType](#_d4v5y17v0b9y) | text | Defines group of text properties that applies on "displayText" content. |

### HorizontalLine

It's a non editable field used to display text or message.

##### horizontal line attributes

| **Attributes** | **Data type** | **Default** | **Description** |
| --- | --- | --- | --- |
| lineWidth(mandatory) | Numeric | 2 | Line width |
| backgroundColor(optional) | String | transparent | The background color |

##

## Enumerations

### Widget Types

| **Value** | **Description** |
| --- | --- |
| label | It's a non editable field used to display text or message. |
| editText | Its an editable text field. |
| checkBox | A checkbox is a specific type of two-states widget that can be either checked or unchecked |
| choiceBox | This shows collection items or options on form. User can pick one or multiple items based on type.. |
| imageBox | Its an field to pick/show an image |
| signatureBox | Form field to get/show gesture signature |
| date | It is an editText with a clickable date picker icon at right end of the editText. This picker icon "onClick" invokes[DatePickerDialog](http://developer.android.com/reference/android/app/DatePickerDialog.html). We can insert Date to editText field through this dialog. |
| time | It is an editText with a clickable time picker icon at right end of the editText. This picker icon "onClick" invokes[TimePickerDialog](http://developer.android.com/reference/android/app/DatePickerDialog.html). We can insert Date to editText field through this dialog. |
| dateTime | Both date and time edittext combination |
| floatingEditText | EditText widget with material design support for floating label. |
| button | A push button widget |
| horizontalLine | A HorizontalLine is just a line on form to indicate separation of two widgets or sections. It draws a line for entire width of form. |
| imageView | Shows image on form screen. It is not editable field |

### Text styles (TextStylesType)

| **Value** | **Descriptions** |
| --- | --- |
| small | textColor="black" fontSize=12sp fontName="Roboto" italic=false bold=false |
| medium | textColor="black" fontSize=20sp fontName="Roboto"italic=false bold=false |
| large | textColor="black" fontSize=24sp fontName="Roboto"italic=false bold=false |
| small\_italic | textColor="black" fontSize=12sp fontName="Roboto"italic=true bold=false |
| medium\_italic | textColor="black" fontSize=20sp fontName="Roboto"italic=true bold=false |
| large\_italic | textColor="black" fontSize=24sp fontName="Roboto"italic=true bold=false |
| small\_bold | textColor="black" fontSize=12sp fontName="Roboto"italic=false bold=true |
| medium\_bold | textColor="black" fontSize=20sp fontName="Roboto"italic=false bold=true |
| large\_bold | textColor="black" fontSize=24sp fontName="Roboto"italic=false bold=true |

### Input text types (InputTextStyle)

| **Value** | **Descriptions** |
| --- | --- |
| text | Regular strings |
| number | Numbers only show number keyboard |
| password | Regular strings , entering texts show as dots |
| passwordNumeric | Numbers only show number keyboard |

### Choice box types (ChoiceBoxType)

| **Value** | **Descriptions** |
| --- | --- |
| list | list of options with checkbox to select |
| spinner | drop down list to select single item |
| radio | single choice - all options always show on screen |

###

### Yes or No options (YesOrNoOptionType)

| **Value** | **Descriptions** |
| --- | --- |
| yes | consider the field as mandatory |
| no | consider the field as optional |

###

### Visible options (VisibleOptionType)

| **Value** | **Descriptions** |
| --- | --- |
| show | Make the widget as visible on screen |
| hide | Remove or hide the widget on screen |

### Arrange order options (ArrangeOrderOptionsType)

| **Value** | **Descriptions** |
| --- | --- |
| sameOrder | Show the list as it is in given order |
| alphabetic | Sort the options based on the alphabetic |
| ascending | Sort the options as ascending order (If all options are numeric) |
| descending | Sort the options as descending order (If all options are numeric) |

### Orientation of section (OrientationType)

| **Value** | **Descriptions** |
| --- | --- |
| horizontal | Arranges its children horizontally inside the section. |
| vertical | Arranges its children vertically inside the section. |

**Relations**